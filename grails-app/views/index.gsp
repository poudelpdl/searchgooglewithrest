<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome</title>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>
<div class="loading" style="display: none;"></div>
<div class="svg" role="presentation">
    <div class="grails-logo-container">
    </div>
</div>
<div id="content" role="main">
    <section class="colset-2-its top30" >
        <div class="container">
            <div class="row">
                <h1>Welcome, Please press the go button to find the total count. </h1>
            </div>
        </div>

    </section>
    <section class="row colset-2-its top30">


        <div class="col-md-4  top30">

        </div>
        <div class="col-md-4 top30">
            <button class="searchProduct">Go</button>
        </div>
        <div class="col-md-4 top30">
        </div>
    </section>
    <section class="row colset-2-its top30" id="search-result-container">
        <div class="col-md-12  top30 result">

        </div>
    </section>
</div>
<asset:javascript src="search.js"/>

</body>
</html>
