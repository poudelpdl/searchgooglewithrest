$(function(){
   searchProduct();
});

var searchProduct = function () {
    $( ".searchProduct" ).click(function() {
        $('.loading').show();
        $.ajax({
            url: '/search/searchProduct',
            success: function (data) {
                $('#search-result-container').html('');
                if(data){
                    $('.loading').hide();
                    console.log("ajax request sucessfull!!");
                    showTotalCountOfURlAppearance(data);
                }
            },
            error: function () {
                alert("There was error in search");
                location.reload();
            }
        });
    });

};
/*
* This method can parse a html response and find
* the required attribute and append the count
* */
var processData = function (dataHtml) {
var getLink = $(dataHtml).find("div.r > a");
var totalCount = 0;
    $( getLink).each(function( i ) {
        if($( this ).attr('href').indexOf('infotrack')> -1 ){
            totalCount += 1
        }
    });
    $('#search-result-container ').html("<p>The Url appeared   " + totalCount +" number of times </p>");
};
/*
*
* This method appends ap tag with total count of Url appearance
* */
var showTotalCountOfURlAppearance = function (count) {
    $('#search-result-container ').html("<p>The Url appeared   " + count +" number of times </p>");
};