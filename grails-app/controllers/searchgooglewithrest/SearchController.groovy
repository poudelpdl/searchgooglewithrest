package searchgooglewithrest

import grails.converters.JSON
import org.apache.commons.lang.exception.ExceptionUtils


class SearchController {
def searchService

    def searchProduct() {
        int totalCount = 0
        try {
            totalCount = searchService.searchAndReturnParsedResponse()
        }catch(Exception e){
            throw new Exception("Exception encountered f"  + ExceptionUtils.getStackTrace(e))
        }
        render  totalCount as String
        return

    }
}
